#ifndef CONS_H
#define CONS_H
#include <stdint.h>
#include <stdbool.h>
#include "type.h"

#define ERRD (data_t){.type=ERR, .data=0}
#define NILD (data_t){.type=NIL, .data=0}

typedef struct data_s {
	enum Type type;
	union {
		char* data_c;
		uint32_t data_i;
		void* data;
	};
	
} data_t;

typedef struct {
	data_t car;
	data_t cdr;
} cons_t;

cons_t* cons(data_t car, data_t cdr);
cons_t* cons_nogc(data_t car, data_t cdr);
void print_cons(cons_t* c);
void print_data(data_t d);

uint32_t get_data_length(data_t data);
uint32_t write_data(char* buffer, data_t data);
data_t read_data(char* buffer, uint32_t* len);

void free_data_nogc(data_t dat);
data_t data_dup(data_t orig);
#endif
