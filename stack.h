#ifndef STACK_H
#define STACK_H
#include <stdbool.h>

#include "cons.h"

data_t* get_stack();
uint32_t get_stack_len();

bool push(data_t val);
data_t pop();
void sw();

void print_stack();
void free_stack();
#endif
