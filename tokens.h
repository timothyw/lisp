#ifndef TOKENS_H
#define TOKENS_H
#include "type.h"
#include "cons.h"

typedef struct tokens_s {
	enum Type type;
	union {
		char* token_c;
		struct tokens_s* token_l;
	};
	struct tokens_s* next;

} tokens_t;

data_t tokenize(char* source, uint32_t* len);
// void print_tok(tokens_t* tok);
// void free_tok(tokens_t* tok);
#endif
