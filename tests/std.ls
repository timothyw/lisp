(define list (lambda a a))
(define-macro defmacro (lambda (name args body) 
	(list 'define-macro name (list 'lambda args body))))
(defmacro defun (name args body) (list 'define name (list 'lambda args body)))

(defun cons? (l)
  (if (not (atom? l))
    1
    0))

(defun list? (l)
  (if (nil? l)
    1
    (if (not (atom? l))
      (if (list? (cdr l))
	1
	0)
      0)))

(defun _length (l n)
  (if (nil? l)
    n
    (_length (cdr l) (+ n 1))))

(defun length (l)
  (_length l 0))

(defun _case (condi cases) 
 (if (nil? cases)
     0
     (let ((fir (car cases)) (val (car fir)) (bod (car (cdr fir))))
       (list 'if (list 'eq? val condi) bod (_case condi (cdr cases))))))

(defmacro case (condi . cases)
 (_case condi cases))

(defun _cond-help (cases)
 (if (nil? cases)
  0
  (let ((cur (car cases)) (ex (car cur)) (b (car (cdr cur))))
   (if (eq? ex 'else)
       b
       (list 'if ex b (_cond-help (cdr cases)))))))

(defmacro cond cases
 (_cond-help cases))

(defun _rev (l1 l2) (if (nil? l1) l2 (_rev (cdr l1) (cons (car l1) l2))))
(defun reverse (l) (_rev l '()))

;;; reduces from right;
(defun reduce-right (f i l)
  (if (nil? l)
    i
    (f (reduce-right f i (cdr l)) (car l))))

(defun _reduce-left (f l acc)
  (if (nil? l)
    acc
    (_reduce-left f (cdr l) (f acc (car l)))))

(defun reduce-left (f i l)
   (_reduce-left f l i))

(defun all (f lst)
  (if (nil? lst)
    1
    (and (f (car lst)) (all f (cdr lst)))))

(defun _string->list (str pos len acc)
  (if (= pos 0)
    acc
    (_string->list str (- pos 1) len (cons (get-char str (- pos 1)) acc))))
(defun string->list (str)
  (_string->list str (str-len str) (str-len str) '()))

(defun true l 1)
(defun false l 0)

(defun check (fl l1)
  (if (and (nil? fl) (nil?  l1))
    1
    (if (or (nil? fl) (nil? l1))
      0
      (if ((car fl) (car l1))
	(check (cdr fl) (cdr l1))
	0))))

