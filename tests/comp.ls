; takes cons or int or string and turns into bytecode
(load "std.ls")
;(load "token.ls")

;;; PROGRAM:
(define instrs '())
(define program-pointer 0)

(defun add-instr (instr op)
  (let ((new (cons instr op)))
    (begin
      (set! instrs (cons new instrs))
      (set! program-pointer (+ program-pointer 1))
      new)))

(defun print-program (progr)
  (if (nil? progr)
    (print "program:")
    (begin
      (print-program (cdr progr))
      (print (car progr)))))

;;; VAR TABLE:
(define vars '(("start" . 101) ("nil?" . 2) ("print" . 1)))

(defun _lookup-var (name table)
  (if (nil? table)
    -1
    (if (== (car (car table)) name)
      (cdr (car table))
      (_lookup-var name (cdr table)))))

(defun lookup-var (name)
  (_lookup-var name vars))

(defun add-var (name)
  (let ((exist (lookup-var name)))
    (if (= exist -1)
      (begin
        (set! vars (cons (cons name (+ (cdr (car vars)) 1)) vars))
	(cdr (car vars)))
      exist)))

;;; COMPILER:
(defun comp-fcall (lst n)
  (if (nil? lst)
    (add-instr 'CALL (- n 1))
    (begin
      (comp (car lst))
      (comp-fcall (cdr lst) (+ n 1)))))

(defun comp-begin (exprs)
  (if (nil? exprs)
    1
    (begin
      (comp (car exprs))
      (if (nil? (cdr exprs))
        1	
        (add-instr 'DROP 0))
      (comp-begin (cdr exprs)))))

(defun comp-let (defs expr)
  (if (nil? defs)
    (comp expr)
    (if (check (list string? true) (car defs))
      (begin
	(comp (car (cdr (car defs))))
	(add-instr 'POP_V (add-var (car (car defs))))
	(comp-let (cdr defs) expr))
      (error "invalid let definition"))))
    ; check def-0, comp, pop_v, comp-let cdr))

(defun comp-func (lst)
  (cond
    ((or (== (car lst) "set!") (== (car lst) "define"))
     (if (check (list string? true) (cdr lst))
       (let ((label (car (cdr lst))) (val (car (cdr (cdr lst)))))
	 (begin
	   (comp val)
	   (if (== (car lst) "define")
	     (add-instr 'POP_V (add-var label))
	     (add-instr 'SET (add-var label)))))
       (error "wrong define")))
    ((== (car lst) "begin")
     (comp-begin (cdr lst)))
    ((== (car lst) "let")
     (if (check (list list? true) (cdr lst))
       (begin
         (add-instr 'PUSH_E 0)
	 (print (car (cdr lst)))
	 (print (car (cdr (cdr lst))))
         (comp-let (car (cdr lst)) (car (cdr (cdr lst))))
         (add-instr 'SW 0)
         (add-instr 'POP_E 0))
       (error "wrong let")))
    ((== (car lst) "if")
     (if (== (length (cdr lst)) 3)
       (let ()
         (begin
	   (comp (car (cdr lst)))
	   (define fjmp (add-instr 'JF 1337))
	   (comp (car (cdr (cdr lst))))
	   (define tjmp (add-instr 'JMP 1337))
	   (set-cdr! fjmp program-pointer)
	   (comp (car (cdr (cdr (cdr lst)))))
	   (set-cdr! tjmp program-pointer)))
       (error "wrong if")))
    ; ((== (car lst) "quote")
    ;  ..)
    ; ((== (car lst) "lambda")
    ;  ..)
    ;; TODO: load and define-macro
    ;; TODO: and, or
    (else (comp-fcall lst 0))))

(defun comp (tok)
 (cond
   ((int? tok) (add-instr 'PUSH_I tok))
   ((string? tok) (add-instr 'PUSH_V (add-var tok)))
   ((not (nil? tok)) (comp-func tok))
   (else (error "err, unrecognized token type"))))

(comp '("print" 12))
(comp '("if" 1 2 3))

(print-program instrs)
