; takes string and turns into cons/string/number...
; to seperate uncomment:
;(load "std.ls")

(defun pos-of-space (str pos)
  (if (>= pos (str-len str))
    pos
    (let ((curr (get-char str pos)))
      (if (or (= curr #\)) (= curr #\ ))
        pos
        (pos-of-space str (+ pos 1))))))

(defun parse-token (str pos)
  (let ((end (pos-of-space str pos)))
    (cons end (sub-string str pos end))))

(defun is-int? (lst)
  (all (lambda (x) (and (>= x #\0) (<= x #\9))) lst))

(defun parse-int (str pos) 
  (if (= (get-char str pos) #\-) 
    (let ((posint (parse-int str (+ pos 1))))
      (cons (car posint) (* -1 (cdr posint))))
    (let ((end (pos-of-space str pos))
	  (sub-str (sub-string str pos end))
	  (sub-lst (string->list sub-str)))
        (if (is-int? sub-lst)
      	  (cons end (reduce-left 
      	    (lambda (acc x) (+ (* acc 10) (- x #\0)))
      	    0
      	    sub-lst))
    	  (cons end sub-str)))))


(defun get-next-ch (str pos)
  (if (= (get-char str pos) #\ )
    (get-next-ch str (+ pos 1))
    (cons pos (get-char str pos))))

(defun parse-list (str pos)
  (let ((nposc (get-next-ch str pos)) (npos (car nposc)) (nchar (cdr nposc)))
    (if (= nchar #\))
      (cons (+ npos 1) '()) ; eat last
      (let ((ntoken (_tokenize str npos)) (ntok (cdr ntoken)) (nextpos (car ntoken))
	    (nlist (parse-list str nextpos)))
	(cons (car nlist) (cons ntok (cdr nlist)))))))

(defun _tokenize (str pos)
 (let ((curr (get-char str pos)))
   (cond
     ((eq? curr #\ ) (_tokenize str (+ pos 1)))
     ((eq? curr #\() (parse-list str (+ pos 1 ))) ; add 1?
     ((or (and (>= curr #\0) (<= curr #\9)) (= curr #\-)) (parse-int str pos))
     (else (parse-token str pos)))))

(defun tokenize (str) (_tokenize str 0))

; (print (tokenize "   123   "))
; (print (tokenize "   1test   "))
; (print (tokenize "   test   "))
; (print (tokenize "(1 2 (-4  test 6 ) k 3)"))
