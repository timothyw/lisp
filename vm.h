#ifndef VM_H
#define VM_H

#include <stdint.h>

#include "instr.h"

bool run_program(instr_t* program, uint32_t start, uint32_t len);
bool run_program_jit(instr_t* program, uint32_t start, uint32_t len);
bool walk_program(instr_t* program, uint32_t start, uint32_t len); 

#endif
