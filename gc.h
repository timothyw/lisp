#ifndef GC_H
#define GC_h
#include <stdbool.h>
#include <stdint.h>

void init_gc(uint32_t buckets, uint32_t max_mem);
void* gc_malloc(uint32_t size);
void gc_collect();
void gc_free_all();
void gc_free();
#endif
