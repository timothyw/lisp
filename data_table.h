#ifndef DATA_TABLE_H
#define DATA_TABLE_H

#include <stdint.h>
#include <stdbool.h>
#include "tokens.h"
#include "cons.h"

uint32_t add_data(data_t  data);
data_t get_data(uint32_t);
uint32_t get_data_table_length();
bool write_data_table(char*);
bool read_data_table(char* buffer, uint32_t len);
void free_data_table();
#endif
