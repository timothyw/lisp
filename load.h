#ifndef LOAD_H
#define LOAD_H

#include <stdbool.h>

bool load_file(char* file_name, bool optimize);
bool write_file(char* file_name);

#endif
