#ifndef INSTR_H
#define INSTR_H

#include <stdint.h>
#include <stdbool.h>

typedef enum {
	NOP = 0,
	PUSH_I,
	PUSH_V,
	POP_V,
	PUSH_E,
	POP_E,
	PUSH_D,
	DROP,

	MAKE_F,

	SET,

	JF,
	JT,
	JMP,

	CALL,
	RET,
	SW,
	CALL_TAIL
} opp_t;

typedef struct {
	opp_t opp_code;
	uint32_t operand;
} instr_t;

int print_instr(instr_t instruction);
uint32_t add_instr(opp_t opcode, uint32_t operand);
void del_instr(uint32_t id);
instr_t* get_program();
uint32_t get_program_pointer();
void set_program_pointer(uint32_t new);
void print_program();
uint32_t get_program_length();
bool write_code(char*);
bool read_code(char* , uint32_t length);

void free_program();
#endif
