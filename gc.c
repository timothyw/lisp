#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "gc.h"
#include "stack.h"
#include "env.h"
#include "func.h"

typedef struct gcptr_s{
	void * ptr;
	uint32_t size;
	bool set;
	bool free;
	//get next in bucket
	struct gcptr_s* next;
} gcptr_t;

typedef struct {
	gcptr_t* first;
	gcptr_t* last;
	//uint32_t count;
} bucket_t;

bucket_t* pointers = NULL;
uint32_t pointers_size = 0;

//size for knowing when to run gc
uint32_t total_size = 0;
uint32_t max_size = 0;

void init_gc(uint32_t size, uint32_t max_mem) {
	pointers_size = size;
	pointers = calloc(size, sizeof(bucket_t));
	max_size = max_mem;
}

void add_ptr(void* ptr, uint32_t size) {
	intptr_t ptr_i = (intptr_t)ptr;
	uint32_t id = ptr_i % pointers_size;

	gcptr_t* new = malloc(sizeof(gcptr_t));
	new->ptr = ptr;
	new->size = size;
	new->set = false;
	new->free = false;
	new->next = NULL;
	if (pointers[id].last) {
		pointers[id].last->next = new;
		pointers[id].last = new;
	} else {
		pointers[id].first = new;
		pointers[id].last = new;
	}
}


void * gc_malloc(uint32_t size) {
	void* ptr = malloc(size);
	if (ptr) {
		add_ptr(ptr, size);
		total_size += size;
	}
	return ptr;
}

// returns if already set...
bool gc_set(void* ptr) {
	uint32_t id = ((intptr_t)ptr) % pointers_size;
	gcptr_t* first = pointers[id].first;
	for (;first; first=first->next) {
		if (ptr == first->ptr) {
			bool ret = first->set;
			first->set = true;
			return ret;
		}
	}
	return true; // non added pointer -> don't free ever!!
}

void gc_add(data_t data) {
	if (data.type == CONS) {
		if (!gc_set(data.data)) {
			cons_t* c = data.data;
			gc_add(c->car);
			gc_add(c->cdr);
		}
	} else if (data.type == ENV) {
		cons_t** env = data.data;
		if (!gc_set(env)) {
			if (!gc_set(*env)) {
				gc_add((*env)->car);
				gc_add((*env)->cdr);
			}
		}
	} else if (data.type == LAMBDA || data.type == MACRO) {
		lambda_t* f = data.data;
		if (!gc_set(f)) {
			gc_add((data_t){.type=ENV, .data=f->closure});
			gc_set(f->args);
		}
	} else if (data.type == STR) {
		//TODO: data strings are not gc??? data cons?
		gc_set(data.data);
	}
}

void gc_collect() {
	if (total_size < max_size)
		return;

	printf("total_size: %d\n", total_size);
	// mark:
	uint32_t len = get_stack_len();
	data_t* stack = get_stack();
	for (uint32_t i = 0; i < len; i++) {
		gc_add(stack[i]);
	}
	gc_add((data_t){.type=ENV, .data=get_current_env()});
	
	// sweep:
	uint32_t freed = 0;
	uint32_t total = 0;
	for (uint32_t i = 0; i < pointers_size; i++) {
		if(pointers[i].first) {
			gcptr_t* next = pointers[i].first;
			gcptr_t* prev = NULL;
			while (next) {
				if(!next->set) {
					total_size -= next->size;
					free(next->ptr);
					next->ptr = NULL;
					gcptr_t* tmp = next->next;
					free(next);
					if (prev) {
						if (tmp == NULL) {
							pointers[i].last = prev;
						}
						prev->next = tmp;
					} else {
						pointers[i].first = tmp;
						if (tmp == NULL) {
							pointers[i].last = NULL;
						}
					}

					next = tmp;
					freed++;
				} else {
					next->set = false;
					prev = next;
					next = next->next;
				}
				total++;
			}
		}
	}
	//printf("objects: %d\n", total);

	//printf("freed: %d\n", freed);
	//printf("new_size: %d\n", total_size);
}

void gc_free_all() {
	printf("still at exit: %d\n", total_size);
	for (uint32_t i = 0; i < pointers_size; i++) {
		if(pointers[i].first) {
			gcptr_t* next = pointers[i].first;
			while (next) {
				free(next->ptr);
				gcptr_t* tmp = next->next;
				free(next);
				next = tmp;
			}
			pointers[i].first = pointers[i].last = NULL;
		}
	}
}
void gc_free() {
	free(pointers);
}
