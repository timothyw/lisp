#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "vm.h"
#include "cons.h"
#include "stack.h"
#include "env.h"
#include "func.h"
#include "builtins.h"
#include "data_table.h"
#include "gc.h"
#include "var_table.h"

typedef bool (*instr_func_t)(uint32_t);

instr_func_t* functions;

uint32_t instr_pointer;

bool call_tail(uint32_t nargs) {
	data_t* args = malloc(nargs*sizeof(data_t));
	for (uint32_t i = 0; i < nargs; i++) {
		args[i] = pop();
	}
	data_t func = pop();
	if (func.type == FUNC) {
		func_t* f = func.data;
		if (f->arg_len != -1 && f->arg_len != nargs) {
			puts("error wrong amount of arguments");
			free(args);
			return false;
		}
		for (uint32_t i = 0; i < nargs/2; i++) {
			data_t tmp = args[i];
			args[i] = args[nargs-i-1];
			args[nargs-i-1] = tmp;
		}
		data_t ret = f->function(args, nargs);
		if (ret.type == ERR) {
			puts("error calling builtins");
			free(args);
			return false;
		}
		push(ret);
		instr_pointer++;
		free(args);
		return true;
	} else if (func.type == LAMBDA) {
		lambda_t *f = func.data;

		cons_t* c = *get_current_env();
		for(; c; c = c->cdr.data) {
			if (c->car.type == ENV) {
				break;
			}
		}
		if (c) {
			*current_env = c;
		} else {
			puts("something might have gone wrong?");
			set_current_env(new_env(f->closure));
		}
		if (f->arg_len < 0) {
			if (nargs < (-f->arg_len) - 1) {
				puts("wrong amount of args");
				free(args);
				return false;
			}
			int32_t cons_args = nargs + f->arg_len + 1;
			int32_t norm_args = (-f->arg_len) - 1;
			
			// create cons
			// set last var
			cons_t* current = NULL;
			for (uint32_t i = 0; i < cons_args; i++) {
				if (current == NULL) {
					current = cons(args[i], (data_t){.type=NIL,.data=0});
				} else {
					current = cons(args[i], \
						(data_t){.type=CONS,.data=current});
				}

			}
			//  0 -> ...
			if (current)
				add_var(f->args[0], (data_t){.type=CONS, .data=current});
			else
				add_var(f->args[0], (data_t){.type=NIL, .data=0});

			// add normal vars
			for (uint32_t i = 0; i < norm_args; i++) {
				add_var(f->args[i+1], args[i+cons_args]);
			}
		} else {
			if (nargs != f->arg_len) {
				puts("wrong amount of arguments!!");
				free(args);
				return false;
			}
			for (uint32_t i = 0; i < f->arg_len; i++) {
				add_var(f->args[i], args[i]);
			}
		}

		instr_pointer = f->addr;
		free(args);
		return true;
	} else {
		printf("%d is not callable", func.type);
		free(args);
		return false;
	}
}

bool call(uint32_t nargs) {
	data_t* args = malloc(nargs*sizeof(data_t));
	for (uint32_t i = 0; i < nargs; i++) {
		args[i] = pop();
	}
	data_t func = pop();
	if (func.type == FUNC) {
		func_t* f = func.data;
		if (f->arg_len != -1 && f->arg_len != nargs) {
			puts("error wrong amount of arguments");
			free(args);
			return false;
		}
		for (uint32_t i = 0; i < nargs/2; i++) {
			data_t tmp = args[i];
			args[i] = args[nargs-i-1];
			args[nargs-i-1] = tmp;
		}
		data_t ret = f->function(args, nargs);
		if (ret.type == ERR) {
			puts("error calling builtins");
			free(args);
			return false;
		}
		push(ret);
		instr_pointer++;
		free(args);
		return true;
	} else if (func.type == LAMBDA) {
		lambda_t *f = func.data;

		data_t addr_d = {.type=INT, .data_i = instr_pointer+1};
		data_t cur_env = {.type=ENV, .data=get_current_env()};
		cons_t* ret_ad = cons(addr_d, cur_env); //nogc???

		push((data_t){.type=CONS, .data=ret_ad});
		set_current_env(new_env(f->closure));
		if (f->arg_len < 0) {
			if (nargs < (-f->arg_len) - 1) {
				puts("wrong amount of args");
				free(args);
				return false;
			}
			int32_t cons_args = nargs + f->arg_len + 1;
			int32_t norm_args = (-f->arg_len) - 1;
			
			// create cons
			// set last var
			cons_t* current = NULL;
			for (uint32_t i = 0; i < cons_args; i++) {
				if (current == NULL) {
					current = cons(args[i], (data_t){.type=NIL,.data=0});
				} else {
					current = cons(args[i], \
						(data_t){.type=CONS,.data=current});
				}

			}
			//  0 -> ...
			if (current)
				add_var(f->args[0], (data_t){.type=CONS, .data=current});
			else
				add_var(f->args[0], (data_t){.type=NIL, .data=0});

			// add normal vars
			for (uint32_t i = 0; i < norm_args; i++) {
				add_var(f->args[i+1], args[i+cons_args]);
			}

		} else {
			if (nargs != f->arg_len) {
				puts("wrong amount of arguments!!");
				free(args);
				return false;
			}
			for (uint32_t i = 0; i < f->arg_len; i++) {
				add_var(f->args[i], args[i]);
			}
		}

		instr_pointer = f->addr;
		free(args);
		return true;
	} else {
		printf("%d is not callable", func.type);
		free(args);
		return false;
	}
}

bool make_f(uint32_t nargs) {
	int32_t n_args = nargs;
	uint32_t * args;
	if (n_args < 0) {
		args = gc_malloc((-n_args)*sizeof(uint32_t));
		for (uint32_t i = 0; i < (-n_args); i++) {
			data_t arg = pop();
			if (arg.type != INT) {
				free(args);
				puts("error in make_f");
				return false;
			}
			args[i] = arg.data_i;
		}
	} else {
		args = gc_malloc(nargs*sizeof(uint32_t));
		for (uint32_t i = 0; i < nargs;i++) {
			data_t arg = pop();
			if (arg.type != INT) {
				free(args);
				puts("error make_f");
				return false;
			}
			args[i] = arg.data_i;
		}
	}
	data_t addr_d = pop();
	if (addr_d.type != INT) {
		free(args);
		puts("error in make_f");
		return false;
	}
	lambda_t* func = gc_malloc(sizeof(lambda_t));
	func->arg_len = n_args;
	func->args = args;
	func->addr = addr_d.data_i;
	func->closure = get_current_env();
	push((data_t){.type=LAMBDA,.data=func});
	instr_pointer++;
	return true;
}

bool ret() {
	data_t adr_c = pop();
	if (adr_c.type != CONS) {
		puts("error in ret");
		return false;
	}
	cons_t* cons_adr = adr_c.data;
	data_t adr_di = cons_adr->car;
	data_t env_d = cons_adr->cdr;
	set_current_env(env_d.data);
	instr_pointer = adr_di.data_i;
	return true;
}

bool nop(uint32_t nop) {
	instr_pointer++;
	return true;
}

bool push_i(uint32_t i) {
	push((data_t){.type=INT, .data_i=i});
	instr_pointer++;
	return true;
}

bool push_v(uint32_t v) {
	data_t var = get_var(*get_current_env(), v);
	if (var.type == ERR) {
		puts("error, undef var");
		printf("var: %s\n", get_var_table(v));
		return false;
	}
	push(var);
	instr_pointer++;
	return true;
}

bool pop_v(uint32_t v) {
	add_var(v, pop());
	instr_pointer++;
	return true;
}

bool push_e(uint32_t e) {
	push((data_t){.type=ENV,.data=get_current_env()}); 
	set_current_env(new_env(get_current_env()));
	instr_pointer++;
	return true;
}

bool pop_e(uint32_t e) {
	data_t tmp = pop();
	if (tmp.type != ENV) {
		puts("error popping env");
		return false;
	}
	set_current_env(tmp.data);
	instr_pointer++;
	return true;
}

bool push_d(uint32_t d) {
	push(get_data(d));
	instr_pointer++;
	return true;
}

bool drop(uint32_t g) {
	pop();
	instr_pointer++;
	return true;
}

bool set(uint32_t v) {
	push(set_var(*get_current_env(), v, pop()));
	instr_pointer++;
	return true;
}

bool jf(uint32_t addr) {
	data_t tmp = pop();
	if(!tmp.data_i)
		instr_pointer = addr;
	else
		instr_pointer++;
	return true;
}

bool jt(uint32_t addr) {
	data_t tmp = pop();
	if (tmp.data_i)
		instr_pointer = addr;
	else
		instr_pointer++;
	return true;
}

bool jmp(uint32_t addr) {
	instr_pointer = addr;
	return true;
}

bool sw_i(uint32_t g) {
	sw();
	instr_pointer++;
	return true;
}

bool exec(instr_t inst) {
	switch (inst.opp_code) {
		case NOP:
			return nop(0);
		case PUSH_I:
			return push_i(inst.operand);
		case PUSH_V:
			return push_v(inst.operand);
		case POP_V:
			return pop_v(inst.operand);
		case PUSH_E:
			return push_e(inst.operand);
		case POP_E:
			return pop_e(inst.operand);
		case PUSH_D:
			return push_d(inst.operand);
		case DROP:
			return drop(inst.operand);
		case MAKE_F:
			return make_f(inst.operand);
		case SET:
			return set(inst.operand);
		case JF:
			return jf(inst.operand);
		case JT:
			return jt(inst.operand);
		case JMP:
			return jmp(inst.operand);
		case CALL:
			return call(inst.operand);
		case RET:
			return ret(0);
		case SW:
			return sw_i(0);
		case CALL_TAIL:	
			return call_tail(inst.operand);
	}
	return true;
}

instr_func_t get_func(uint32_t  opp_code) {
	switch (opp_code) {
		case NOP:
			return nop;
		case PUSH_I:
			return push_i;
		case PUSH_V:
			return push_v;
		case POP_V:
			return pop_v;
		case PUSH_E:
			return push_e;
		case POP_E:
			return pop_e;
		case PUSH_D:
			return push_d;
		case DROP:
			return drop;
		case MAKE_F:
			return make_f;
		case SET:
			return set;
		case JF:
			return jf;
		case JT:
			return jt;
		case JMP:
			return jmp;
		case CALL:
			return call;
		case RET:
			return ret;
		case SW:
			return sw_i;
		case CALL_TAIL:
			return call_tail;
	}
	return NULL;
}

//TODO: make bool so error can be detected

bool walk_program(instr_t* program, uint32_t start, uint32_t len) {
	instr_pointer = start;
	while(instr_pointer < len) {
		instr_t instr = program[instr_pointer];
		
		print_instr(instr);
		getchar();
		if (!exec(instr)) return false;
		gc_collect();

		printf("env = (");
		print_cons(*get_current_env());
		printf(")\n");
		printf("stack:  \n");
		print_stack();
	}
	return true;
}

bool run_program(instr_t* program, uint32_t start, uint32_t len) {
	instr_pointer = start;
	uint32_t gc_t = 1;
	while(instr_pointer < len) {
		instr_t instr = program[instr_pointer];
		if(!exec(instr)) {
			puts("error!!!");
			return false;
		}
		if (gc_t % 100 == 0) {
			gc_collect();
		}
		gc_t++;
	}
	return true;
}

bool run_program_jit(instr_t* program, uint32_t start, uint32_t len) {
	// build functions
	functions = malloc(len*sizeof(instr_func_t));
	for (uint32_t i = 0; i < len; i++) {
		functions[i] = get_func(program[i].opp_code);
	}


	instr_pointer = start;
	uint32_t gc_t = 1;
	while (instr_pointer < len) {
		bool ret = functions[instr_pointer](program[instr_pointer].operand);
		if (!ret) {
			puts("error!");
			free(functions);
			return false;
		}
		if (gc_t % 100 == 0)
			gc_collect();
		gc_t++;
	}
	free(functions);
	return true;
}
