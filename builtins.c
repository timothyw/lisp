#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "builtins.h"
#include "env.h"
#include "var_table.h"
#include "gc.h"

#define MAKE_BUILTIN(x, n) func_t x ## _f = {.arg_len = n, .function=&x}; \
			data_t builtin_ ## x = {.type=FUNC, .data=&(x  ## _f)};

data_t print(data_t* args, uint32_t nargs) {
	if (args[0].type == STR) {
		printf("%s\n", args[0].data_c);
	} else {
		print_data(args[0]);
		printf("\n");
	}
	return (data_t){.type=INT, .data_i=-1};
}
MAKE_BUILTIN(print, 1);

data_t Bcons(data_t* args, uint32_t nargs) {
	return (data_t){.type=CONS, .data=cons(args[0], args[1])};
}
MAKE_BUILTIN(Bcons, 2);

data_t car(data_t* args, uint32_t nargs) {
	if (args[0].type != CONS) {
		return (data_t){.type=ERR, .data=0};
	}
	return ((cons_t*)args[0].data)->car;
}
MAKE_BUILTIN(car, 1);

data_t cdr(data_t* args, uint32_t nargs) {
	if (args[0].type != CONS) {
		return (data_t){.type=ERR, .data=0};
	}
	return ((cons_t*)args[0].data)->cdr;
}
MAKE_BUILTIN(cdr, 1);

data_t atom(data_t* args, uint32_t nargs) {
	if (args[0].type == CONS) {
		return (data_t){.type=INT, .data_i=0};
	} else {
		return (data_t){.type=INT, .data_i=1};
	}
}
MAKE_BUILTIN(atom, 1);

data_t intr(data_t* args, uint32_t nargs) {
	return (data_t){.type=INT, .data_i=(args[0].type == INT)};
}
MAKE_BUILTIN(intr, 1);

data_t eq(data_t* args, uint32_t nargs) {
	return (data_t){.type=INT, .data_i=(args[0].data == args[1].data)};
}
MAKE_BUILTIN(eq, 2);

data_t nil(data_t* args, uint32_t nargs) {
	return (data_t){.type=INT, .data_i= args[0].type == NIL};
}
MAKE_BUILTIN(nil, 1);

data_t string(data_t* args, uint32_t narg) {
	return (data_t){.type=INT, .data_i = args[0].type == STR};
}
MAKE_BUILTIN(string, 1);

data_t add(data_t* args, uint32_t nargs) {
	uint32_t ret = 0;
	for (uint32_t i = 0; i < nargs; i++) {
		if (args[i].type != INT)
			return (data_t){.type=ERR, .data=0};
		ret += args[i].data_i;
	}
	return (data_t){.type=INT, .data_i=ret};
}
MAKE_BUILTIN(add, -1);

data_t sub(data_t* args, uint32_t nargs) {
	if (nargs < 1 || args[0].type != INT)
		return (data_t){.type=INT, .data=0};

	uint32_t ret = args[0].data_i;
	for (uint32_t i = 1; i < nargs; i++) {
		if (args[i].type != INT)
			return (data_t){.type=ERR, .data=0};
		ret -= args[i].data_i;
	}
	return (data_t){.type=INT, .data_i=ret};
}
MAKE_BUILTIN(sub, -1);

data_t mul(data_t* args, uint32_t nargs) {
	uint32_t ret = 1;
	for (uint32_t i = 0; i < nargs; i++) {
		if (args[i].type != INT)
			return (data_t){.type=ERR, .data=0};
		ret *= args[i].data_i;
	}
	return (data_t){.type=INT, .data_i=ret};
}
MAKE_BUILTIN(mul, -1);

data_t div(data_t* args, uint32_t nargs) {
	if (nargs < 1 || args[0].type != INT)
		return (data_t){.type=INT, .data=0};

	uint32_t ret = args[0].data_i;
	for (uint32_t i = 1; i < nargs; i++) {
		if (args[i].type != INT)
			return (data_t){.type=ERR, .data=0};
		ret /= args[i].data_i;
	}
	return (data_t){.type=INT, .data_i=ret};
}
MAKE_BUILTIN(div, -1);

uint32_t equals_help(data_t first, data_t sec) {
	// check type
	// if cons:
	// 	check car equals
	// 	check cdr equals
	// if string:
	// 	strcmp
	// else:
	// 	data == data
	if (first.type != sec.type) {
		return 0;
	} else if (first.type == CONS) {
		cons_t* cons1 = first.data;
		cons_t* cons2 = sec.data;
		uint32_t eq1 = equals_help(cons1->car, cons2->car);
		uint32_t eq2 = equals_help(cons1->cdr, cons2->cdr);
		return eq1&&eq2;
	} else if (first.type == STR) {
		return strcmp(first.data, sec.data) == 0;
	} else {
		return first.data == sec.data;
	}
}

data_t equals(data_t* args, uint32_t nargs) {
	return (data_t){.type=INT, .data_i=equals_help(args[0], args[1])};
}

MAKE_BUILTIN(equals, 2);

//TODO: fix sign?
data_t le(data_t* args, uint32_t nargs) {
	return (data_t){.type=INT, .data_i=(args[0].data_i)<=(args[1].data_i)};
}
MAKE_BUILTIN(le, 2);

data_t ge(data_t* args, uint32_t nargs) {
	return (data_t){.type=INT, .data_i=(args[0].data_i)>=(args[1].data_i)};
}
MAKE_BUILTIN(ge, 2);

data_t lt(data_t* args, uint32_t nargs) {
	return (data_t){.type=INT, .data_i=(args[0].data_i)<(args[1].data_i)};
}
MAKE_BUILTIN(lt, 2);

data_t gt(data_t* args, uint32_t nargs) {
	return (data_t){.type=INT, .data_i=(args[0].data_i)>(args[1].data_i)};
}
MAKE_BUILTIN(gt, 2);

data_t not(data_t* args, uint32_t nargs) {
	return (data_t){.type = args[0].type, .data_i = !args[0].data_i};
}
MAKE_BUILTIN(not, 1);


//strings:
data_t get_char(data_t* args, uint32_t nargs) {
	if (args[0].type != STR || args[1].type != INT) {
		return ERRD;
	}
	return (data_t){.type=INT, .data_i=args[0].data_c[args[1].data_i]};
}
MAKE_BUILTIN(get_char, 2);

data_t sub_string(data_t* args, uint32_t nargs) {
	data_t str_d = args[0];
	data_t start_d = args[1];
	data_t end_d = args[2];
	if (str_d.type != STR || start_d.type != INT || end_d.type != INT) {
		return ERRD;
	}
	char* str = str_d.data;
	uint32_t start = start_d.data_i;
	uint32_t end = end_d.data_i;
	uint32_t len = strlen(str);
	if (start > end || end > len) {
		return ERRD;
	}
	char* ret = gc_malloc(end-start+1);
	memcpy(ret, str+start, end-start);
	ret[end-start] = '\0';
	return (data_t){.type=STR, .data=ret};
}
MAKE_BUILTIN(sub_string, 3);

data_t str_len(data_t* args, uint32_t nargs) {
	if (args[0].type != STR) {
		return ERRD;
	}
	return (data_t){.type=INT, .data_i=strlen(args[0].data)};
}
MAKE_BUILTIN(str_len, 1);

data_t set_car(data_t* args, uint32_t nargs) {
	if (args[0].type != CONS) {
		return ERRD;
	}
	cons_t* c = args[0].data;
	c->car = args[1];
	return (data_t){.type=INT, .data_i=-1};
}
MAKE_BUILTIN(set_car, 2);

data_t set_cdr(data_t* args, uint32_t nargs) {
	if (args[0].type != CONS) {
		return ERRD;
	}
	cons_t* c = args[0].data;
	c->cdr = args[1];
	return (data_t){.type=INT, .data_i=-1};
}
MAKE_BUILTIN(set_cdr, 2);

void init_builtins()  {
	add_var(lookup_var("print"), builtin_print);
	add_var(lookup_var("cons"), builtin_Bcons);
	add_var(lookup_var("car"), builtin_car);
	add_var(lookup_var("cdr"), builtin_cdr);
	add_var(lookup_var("atom?"), builtin_atom);
	add_var(lookup_var("int?"), builtin_intr);
	add_var(lookup_var("eq?"), builtin_eq);
	add_var(lookup_var("nil?"), builtin_nil);
	add_var(lookup_var("string?"), builtin_string);
	add_var(lookup_var("+"), builtin_add);
	add_var(lookup_var("-"), builtin_sub);
	add_var(lookup_var("*"), builtin_mul);
	add_var(lookup_var("/"), builtin_div);
	add_var(lookup_var("="), builtin_eq);
	add_var(lookup_var("=="), builtin_equals);
	add_var(lookup_var("equals?"), builtin_equals);
	add_var(lookup_var("<="), builtin_le);
	add_var(lookup_var(">="), builtin_ge);
	add_var(lookup_var("<"), builtin_lt);
	add_var(lookup_var(">"), builtin_gt);
	add_var(lookup_var("not"), builtin_not);
	add_var(lookup_var("get-char"), builtin_get_char);
	add_var(lookup_var("sub-string"), builtin_sub_string);
	add_var(lookup_var("str-len"), builtin_str_len);
	add_var(lookup_var("set-car!"), builtin_set_car);
	add_var(lookup_var("set-cdr!"), builtin_set_cdr);
}
