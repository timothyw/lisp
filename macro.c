#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "macro.h"
#include "var_table.h"
#include "instr.h"
#include "func.h"
#include "compiler.h"
#include "data_table.h"
#include "load.h"
#include "vm.h"
#include "env.h"
#include "stack.h"

#define OK (data_t){.type=INT, .data_i=1}

#define MAKE_MACRO(x, n) func_t x ## _f = {.arg_len = n, .function=&x}; \
                        data_t x ## _macro = {.type=SPECIAL, .data=&(x  ## _f)};

data_t define(data_t* args, uint32_t nargs) {
	if (args[0].type != SYMBOL)
		return ERRD;
	uint32_t start = get_program_pointer();
	uint32_t id = args[0].data_i;
	if (!comp(args[1])) return ERRD;
	add_instr(POP_V, id);
	add_instr(PUSH_I, id);
	// for macros:
	run_program(get_program(), start, get_program_pointer());
	//TODO: remove push_i
	pop();
	return OK;
}

MAKE_MACRO(define, 2)

data_t setm(data_t* args, uint32_t nargs) {
	if (args[0].type != SYMBOL)
		return ERRD;
	uint32_t start = get_program_pointer();
	uint32_t id = args[0].data_i;
	if (id == 0) {
		puts("wrong set");
		return ERRD;
	}
	if (!comp(args[1])) return ERRD;
	add_instr(SET, id);
	// for macro:
	run_program(get_program(), start, get_program_pointer());
	pop();
	return OK;
}

MAKE_MACRO(setm, 2)

data_t begin(data_t* args, uint32_t nargs) {
	for (uint32_t i = 0; i < nargs; i++) {
		if (!comp(args[i])) return ERRD;
		if (i != nargs-1)
			add_instr(DROP, 0);
	}
	return OK;
}

MAKE_MACRO(begin, -1)

//TODO: do lets over macro work...
data_t let(data_t* args, uint32_t nargs) {
	data_t bindings = args[0];
	data_t body = args[1];
	if (bindings.type != CONS && bindings.type != NIL)  {
		puts("invalid let");
		return ERRD;
	}
	add_instr(PUSH_E, 0);
	data_t cur = bindings;
	for(; cur.type != NIL;) {
		if (cur.type != CONS)
			return ERRD;
		cons_t* cur_c = cur.data;
		if (cur_c->car.type != CONS)
			return ERRD;
		cons_t* name_c= cur_c->car.data;
		data_t name = name_c->car;
		if (name_c->cdr.type != CONS || name.type != SYMBOL)
			return ERRD;
		cons_t* value_c = name_c->cdr.data;
		data_t value = value_c->car;
		if (value_c->cdr.type != NIL)
			return ERRD;
		uint32_t id = name.data_i;
		if (!comp(value)) return ERRD;
		add_instr(POP_V, id);
		cur = cur_c->cdr;
	}
	if (!comp(body)) return ERRD;
	add_instr(SW, 0);
	add_instr(POP_E, 0);
	return OK;
}

MAKE_MACRO(let, 2)

data_t ifm(data_t* args, uint32_t nargs) {
	data_t cond = args[0];
	data_t body_t = args[1];
	data_t body_f = args[2];
	if (!comp(cond)) return ERRD;
	uint32_t ERRD_jmp = add_instr(JF, 0);
	if (!comp(body_t)) return ERRD;
	uint32_t OK_jmp = add_instr(JMP, 0);
	get_program()[ERRD_jmp].operand = get_program_pointer();
	if (!comp(body_f)) return ERRD;
	get_program()[OK_jmp].operand = get_program_pointer();
	return OK;

}

MAKE_MACRO(ifm, 3)

data_t quote(data_t* args, uint32_t nargs) {
	//TODO: symbol lookup.
	uint32_t id = add_data(data_dup(args[0]));
	add_instr(PUSH_D, id);
	return OK;
}

MAKE_MACRO(quote, 1)

data_t lambda(data_t* args, uint32_t nargs) {
	data_t arguments = args[0];
	data_t body = args[1];
	uint32_t jmp = add_instr(JMP, 0);
	uint32_t func = get_program_pointer();

	if (!comp(body)) return ERRD;
	add_instr(SW, 0);
	add_instr(RET, 0);
	uint32_t end = add_instr(PUSH_I, func);

	// for each args
	data_t cur = arguments;
	uint32_t n_args = 0;
	for (;cur.type != NIL; n_args++) {
		if( cur.type != CONS) {
			uint32_t id = cur.data_i;
			add_instr(PUSH_I, id);
			n_args = (n_args+1)*-1;
			break;
		}
		cons_t* cur_c = cur.data;
		if (cur_c->car.type != SYMBOL)
			return ERRD;
		uint32_t id = cur_c->car.data_i;
		add_instr(PUSH_I, id);
		
		cur = cur_c->cdr;
	}

	add_instr(MAKE_F, n_args);
	get_program()[jmp].operand = end;
	return OK;
}

MAKE_MACRO(lambda, 2)

data_t load(data_t* args, uint32_t nargs) {
	if (args[0].type != STR) {
		return ERRD;
	}
	char* fname = args[0].data;
	//TODO: find relative path someway??
	if (!load_file(fname, false))
		return ERRD;
	add_instr(PUSH_I, 0);
	return OK;
}

MAKE_MACRO(load, 1)


data_t definemacro(data_t* args, uint32_t nargs) {
	data_t name_d = args[0];
	if (name_d.type != SYMBOL) return ERRD;
	uint32_t id = name_d.data_i;
	data_t func_d = args[1];
	add_instr(NOP, MACRO);
	uint32_t jmp = add_instr(JMP, 0);
	uint32_t start = get_program_pointer();
	if (!comp(func_d)) return ERRD;
	run_program(get_program(), start, get_program_pointer());
	data_t l = pop();
	if (l.type != LAMBDA) {
		puts("define-macro need lambda");
		return ERRD;
	}
	l.type = MACRO;
	add_var(id, l);
	get_program()[jmp].operand = get_program_pointer();
	add_instr(PUSH_I, -1);
	return OK;
	//add jmp???
	// ^ optimize out later???
	//comp func_d
	//run func_d...
	//pop 
	//check for lambda
	//change type
	//add to env
}

MAKE_MACRO(definemacro, 2)

data_t and(data_t* args, uint32_t nargs) {
	if (nargs == 0) {
		add_instr(PUSH_I, 1);
		return OK;
	}
	uint32_t* jmp_ts = malloc(nargs*sizeof(uint32_t));
	for (uint32_t i = 0; i < nargs; i++) {
		if (!comp(args[i])) return ERRD;
		jmp_ts[i] = add_instr(JF, 0);
	}
	add_instr(PUSH_I, 1);
	uint32_t jmp_e = add_instr(JMP, 0);
	uint32_t end_t = add_instr(PUSH_I, 0);
	for (uint32_t i = 0; i < nargs; i++) {
		get_program()[jmp_ts[i]].operand = end_t;
	}
	free(jmp_ts);
	get_program()[jmp_e].operand = get_program_pointer();
	return OK;
}

MAKE_MACRO(and, -1)

data_t or(data_t* args, uint32_t nargs) {
	if (nargs == 0) {
		add_instr(PUSH_I, 0);
		return OK;
	}
	uint32_t* jmp_ts = malloc(nargs*sizeof(uint32_t));
	for (uint32_t i = 0; i < nargs; i++) {
		if (!comp(args[i])) return ERRD;
		jmp_ts[i] = add_instr(JT, 0);
	}
	add_instr(PUSH_I, 0);	
	uint32_t jmp_e = add_instr(JMP, 0);
	uint32_t end_t = add_instr(PUSH_I, 1);
	for (uint32_t i = 0; i < nargs; i++) {
		get_program()[jmp_ts[i]].operand = end_t;
	}
	free(jmp_ts);
	get_program()[jmp_e].operand = get_program_pointer();
	return OK;
}

MAKE_MACRO(or, -1)

void init_specials() {
	add_var(lookup_var("define"), define_macro);
	add_var(lookup_var("set!"), setm_macro);
	add_var(lookup_var("begin"), begin_macro);
	add_var(lookup_var("let"), let_macro);
	add_var(lookup_var("if"), ifm_macro);
	add_var(lookup_var("quote"), quote_macro);
	add_var(lookup_var("lambda"), lambda_macro);
	add_var(lookup_var("load"), load_macro);
	add_var(lookup_var("define-macro"), definemacro_macro);
	//TODO: add \ as lambda?
	add_var(lookup_var("and"), and_macro);
	add_var(lookup_var("or"), or_macro);
}
