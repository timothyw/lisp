#include <string.h>
#include <stdlib.h>

#include "data_table.h"
#include "var_table.h"

data_t* data_table = NULL;
uint32_t data_table_size = 0;
uint32_t data_pointer = 0;


uint32_t add_data(data_t token) {
	if (data_pointer + 5 > data_table_size) {
		data_table_size = data_table_size * 2 + 10;
		data_t* new_table = realloc(data_table, data_table_size*sizeof(data_t));
		if (new_table == NULL)
			return 0;
		data_table = new_table;
	}
	data_table[data_pointer] = token;
	uint32_t ret = data_pointer;
	data_pointer++;
	return ret;
}

data_t get_data(uint32_t id) {
	if (id >= data_pointer) {
		return (data_t){0};	
	}
	return data_table[id];
}

uint32_t get_data_table_length() {
	uint32_t len = sizeof(uint32_t);
	for (uint32_t i = 0; i < data_pointer; i++) {
		len += get_data_length(data_table[i]);
	}
	return len;
}


bool write_data_table(char* buffer) {
	memcpy(buffer, &data_pointer, sizeof(uint32_t));
	uint32_t pos = sizeof(uint32_t);
	for (uint32_t i = 0; i < data_pointer; i++) {
		pos += write_data(buffer+pos, data_table[i]);
	}
	return true;
}

bool read_data_table(char* buffer, uint32_t length) {
	uint32_t data_len = 0;
	memcpy(&data_len, buffer, sizeof(uint32_t));
	data_pointer = data_len;
	data_table_size = data_len;
	data_table = malloc(data_len*sizeof(data_t));

	uint32_t pos = sizeof(uint32_t);
	for(uint32_t i = 0; i < data_len; i++) {
		uint32_t tmp = 0;
		data_table[i] = read_data(buffer+pos, &tmp);
		pos += tmp;
	}
	return true;
}

void free_data_table() {
	for (uint32_t i = 0; i < data_pointer; i++) {
		free_data_nogc(data_table[i]);
	}
	free(data_table);
}
