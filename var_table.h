#ifndef VAR_TABLE_H
#define VAR_TABLE_H

#include <stdint.h>
#include <stdbool.h>

uint32_t add_var_table(char* name);
uint32_t lookup_var(char* name);
char* get_var_table(uint32_t id);

uint32_t get_var_length();
bool write_var_table(char* buffer);
bool read_var_table(char* buffer);
void free_var_table();
#endif
