#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>

#include "instr.h"
#include "tokens.h"
#include "data_table.h"
#include "var_table.h"
#include "compiler.h"
#include "optimize.h"
#include "load.h"

#define DEBUG

bool load_file(char* file_name, bool optimize_flag) {
	char old_cwd[128];
	if (getcwd(old_cwd, 128) == NULL) {
		puts("error saving cwd");
		return false;
	}
	printf("current dir: %s\n", old_cwd);

	FILE* fp_in = fopen(file_name, "rb");
	if (fp_in == NULL) {
		puts("error opening file");
		return false;
	}
	fseek(fp_in, 0, SEEK_END);
	uint32_t in_length = ftell(fp_in);
	rewind(fp_in);

	char* in_buffer = malloc(in_length+4);
	fread(in_buffer, 1, in_length, fp_in);
	fclose(fp_in);

	char* dir = strdup(file_name);
	uint32_t i = strlen(dir)-1;
	for (;i > 0; i--) {
		if (dir[i] == '/') {
			dir[i+1] = '\0';
			break;
		}
	}
	if (i != 0)  {
		printf("cd %s\n", dir);
		if (chdir(dir) != 0) {
			puts("error cd");
			return false;
		}
	}
	free(dir);


	uint32_t check = 0;
	memcpy(&check, in_buffer, sizeof(uint32_t));
	if (check != 12) {
		in_buffer[in_length] = 0;
		uint32_t i = 0;
		bool in_comment = false;
		//TODO: read ';' and ignore uintil '\n'
		for (i = 0; i < in_length; i++) {
			switch (in_buffer[i]) {
				case ';':
					in_comment = true;
					break;
				case '\n':
					in_comment = false;
				case '\t':
				case '\r':
					in_buffer[i] = ' ';
					break;
			}
			if (in_comment)
				in_buffer[i] = ' ';
		}
		i = 0;
		while(in_buffer[i] == ' ') i++;
#ifdef DEBUG
		printf("-----\n%s\n--------\n", in_buffer + i);
#endif
		uint32_t pos = i;

		uint32_t prog_length = in_length;
		while(in_buffer[prog_length-1] == ' ') prog_length--;

		while (pos < prog_length) {
			uint32_t len = in_length - pos;
			data_t tokens = tokenize(in_buffer + pos, &len);
			if (tokens.type == ERR) {
				puts("ERROR parsing");
				return false;
			}
#ifdef DEBUG
			print_data(tokens);
			printf("\n");
#endif
			pos += len;
			if (!comp(tokens)) {
				puts("ERROR compiling");
				return false;
			}
			add_instr(DROP, 0);
			free_data_nogc(tokens);
		}
		if (optimize_flag) optimize();
#ifdef DEBUG
		print_program();
#endif
		
	} else {
		uint32_t var_pointer = check;
		uint32_t data_pointer = 0;
		uint32_t code_pointer = 0;
		memcpy(&data_pointer, in_buffer+sizeof(uint32_t), sizeof(uint32_t));
		memcpy(&code_pointer, in_buffer+2*sizeof(uint32_t), sizeof(uint32_t));
		read_var_table(in_buffer+var_pointer);
		read_data_table(in_buffer+data_pointer, in_length-data_pointer);
		uint32_t len = (in_length-code_pointer)/(sizeof(uint8_t)+sizeof(uint32_t));
		read_code(in_buffer+code_pointer, len);

	}
	free(in_buffer);
	//TODO: change cwd back.
	chdir(old_cwd);
	return true;
}

bool write_file(char* file_name) {
	//calc buffer length
	uint32_t code_len = get_program_length();
	uint32_t var_len = get_var_length();
	uint32_t data_len = get_data_table_length();
	uint32_t pointer_len = 3*sizeof(uint32_t);
	//alloc buffer
	char* buffer = malloc(pointer_len+var_len+code_len+data_len);
	//write tables
	write_var_table(buffer+pointer_len);
	write_data_table(buffer+pointer_len+var_len);
	write_code(buffer+var_len+data_len+pointer_len);
	//write pointers:
	memcpy(buffer, &pointer_len, sizeof(uint32_t));
	uint32_t data_pos = pointer_len+var_len;
	memcpy(buffer+sizeof(uint32_t), &data_pos, sizeof(uint32_t));
	uint32_t code_pos = data_pos+data_len;
	memcpy(buffer+2*sizeof(uint32_t), &code_pos, sizeof(uint32_t));
	FILE* op = fopen(file_name, "wb");
	fwrite(buffer, 1, code_len+var_len+data_len+pointer_len, op);
	fclose(op);
	free(buffer);
	return true;
}
