#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <stdint.h>

#include "vm.h"
#include "compiler.h"
#include "instr.h"
#include "macro.h"
#include "var_table.h"
#include "data_table.h"
#include "env.h"
#include "builtins.h"
#include "stack.h"
#include "gc.h"
#include "optimize.h"
#include "load.h"

#include "tokens.h"
#include "cons.h"

#define MEM 50000
#define BUCK 100


void repl() {
	char buffer[1024] = {0};
	bool walk = false;
	init_gc(BUCK, MEM);
	set_current_env(new_env(NULL));
	init_specials();
	init_builtins();
	printf("> ");
	uint32_t last = 0;
	while (fgets(buffer, 1023, stdin)) {
		uint32_t len = strlen(buffer);
		if (buffer[len-1] == '\n')
			buffer[len-1] = 0;
		if (strcmp(buffer, ":w") == 0) {
			walk = !walk;
			printf("> ");
			continue;
		}
		data_t  tokens = tokenize(buffer, &len);
		if (tokens.type == ERR) {
			puts("ERROR parsing");
			printf("> ");
			continue;
		}
		//print_data(tokens);
		if (!comp(tokens)) {
			puts("ERROR compiling");
			printf("> ");
			continue;
		}
		//print_program();
		free_data_nogc(tokens);
		uint32_t cur = get_program_pointer();
		if (walk) {
			walk_program(get_program(), last, cur);
		} else {
			run_program(get_program(), last, cur);
		}
		last = cur;
		print_data(pop());

		printf("\n");
		printf("> ");
	}
	gc_free_all();
	printf("\n");
}

// usage:
// l - repl
// l <file name> - run lisp program
// l --comp <file name> - compile lisp to bytecode
// l --walk <file name> - walk lisp program
int main(int argc, char* argv[]) {
	bool walk_flag = false;
	bool comp_flag = false;
	if (argc == 1) {
		repl();
		return 0;
	}
	if (argc == 3) {
		if (strcmp(argv[1], "--comp") == 0) {
			comp_flag = true;
		} else if (strcmp(argv[1], "--walk") == 0) {
			walk_flag = true;
		}
	}


	char* file_name = argv[argc-1];
	// create comp env
	// unnesecsdf when loading bytecode...
	init_gc(BUCK, MEM);
	set_current_env(new_env(NULL));
	init_specials();
	init_builtins();
	if (!load_file(file_name, true)) {
		return -1;
	}
	// free env
	gc_free_all();
	// free_macros();
	if (comp_flag) {
		//generate out name:
		size_t len = strlen(file_name);
		char * out_name = malloc(len+2);
		memcpy(out_name, file_name, len);
		out_name[len] = 'c';
		out_name[len+1] = '\0';
		//open file for write
		write_file(out_name);

		gc_free();
		free(out_name);
		free_var_table();
		free_stack();
		free_program();
		free_data_table();
		return 0;
	}
	//run
	set_current_env(new_env(NULL));
	init_builtins();
	if (walk_flag) {
		walk_program(get_program(), 0, get_program_pointer());
	} else {
		run_program_jit(get_program(), 0, get_program_pointer());
	}

	gc_free_all();
	gc_free();
	free_stack();
	free_var_table();
	free_program();
	free_data_table();
	return 0;
}
