#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>

#include "tokens.h"
#include "var_table.h"

char get_next_char(char* buffer, uint32_t* pos) {
	while(buffer[*pos] == ' ') *pos += 1;
	return buffer[*pos];
}

bool is_integer(char* buffer, uint32_t len) {
	uint32_t i = 0;
	if (buffer[0] == '-') {
		i++;
	}
	for (; i < len; i++) {
		if (buffer[i] < '0' || buffer[i] > '9') {
			return false;
		}
	}
	if (i == 1 && buffer[0] == '-')
		return false;
	return true;
}

data_t tokenize(char* source, uint32_t* len) {
	if (*len < 1) return ERRD;
	uint32_t pos = 0;
	char next = get_next_char(source, &pos);
	if (next == '(') {
		cons_t* first = NULL;
		cons_t* current = NULL;
		pos += 1;
		next = get_next_char(source, &pos);
		while (pos < *len) {
			if (next == ')') {
				*len = pos+1;
				if (first == NULL) {
					return NILD;
				}
				return (data_t){.type=CONS,.data=first};
			} else if ( next == '.') {
				// get next
				pos += 1;
				uint32_t sublen = *len - pos;
				data_t n = tokenize(source+pos, &sublen);
				if (n.type == ERR) return n;
				pos += sublen;
				char next = get_next_char(source, &pos);
				if (next != ')' || first == NULL)  {
					puts("wrong dot cons");
					return ERRD;
				}
				current->cdr = n;
				*len = pos+1;
				return (data_t){.type=CONS,.data=first};
				// check if next is ')'
				// return cons
			} else {
				uint32_t sublen = *len - pos;
				data_t n = tokenize(source+pos, &sublen);
				pos += sublen;
				if (n.type == ERR) return n;
				if (first == NULL) {
					first = cons_nogc(n, NILD);
					current = first;
				} else {
					cons_t* next = cons_nogc(n, NILD);
					current->cdr = (data_t){.type=CONS, .data=next};
					current = next;
				}
			}
			next = get_next_char(source, &pos);
		}

		puts("unmatched brackets");
		return ERRD;
	} else if (next == '\'') {
		pos += 1;
 		uint32_t nlen = *len - pos;
 		data_t next = tokenize(source+pos, &nlen);
 		if (next.type == ERR)
 			return next;
 		pos += nlen;
		data_t quote = (data_t){.type=SYMBOL, .data_i=lookup_var("quote")};
		data_t q_c = {.type=CONS, .data=cons_nogc(next, NILD)};
		cons_t* q = cons_nogc(quote, q_c); 
 		 
 		*len = pos;
 		return (data_t){.type=CONS, .data=q};
	} else if (next == '"') {
		uint32_t str_len = 0;
		while(pos+ str_len+1 < *len && source[pos+str_len+1] != '"') str_len++;
		char* buffer = malloc(str_len+1);
		memcpy(buffer, source+pos+1, str_len);
		buffer[str_len] = 0;
		*len = pos+1+str_len+1; // eat closeing "
		return (data_t){.type=STR, .data=buffer};
	} else if(next == '#') {
		if (source[pos+1] == '\\') {
			*len = pos+3;
			return (data_t){.type=INT, .data_i=source[pos+2]};
		}
		return (data_t){.type=ERR};

	}else {
		uint32_t str_len = 1;
		while(pos+str_len < *len && source[pos+str_len] != ' ' && source[pos+str_len] != ')' && source[pos+str_len] != '\0') str_len++;
		if (is_integer(source+pos, str_len)) {
			int32_t integer = 0;
			uint32_t i = 0;
			if (source[pos] == '-') {
				i = 1;
			}
			for (; i < str_len; i++) {
				integer *= 10;
				integer += source[pos+i] - '0';
			}
			if (source[pos] == '-') {
				integer *= -1;
			}
			*len = pos+str_len;
			return (data_t){.type=INT, .data_i=integer};
		}
		char* str = malloc(str_len+1);
		memcpy(str, source+pos, str_len);
		str[str_len] = '\0';
		uint32_t id = add_var_table(str);
		if (id == 0) {
			puts("error while parsing file, ???");
			free(str);
			return ERRD;
		}
		free(str);
		*len = pos+str_len;
		return (data_t){.type=SYMBOL, .data_i=id};
	}
}
