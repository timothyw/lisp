#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "instr.h"

instr_t* program = NULL;
uint32_t program_pointer = 0;
uint32_t program_len = 0;

int print_instr(instr_t instruction) {
	opp_t opp_code = instruction.opp_code;
	uint32_t operand = instruction.operand;
	switch (opp_code) {
		case NOP:
			printf("nop\n");
			break;
		case PUSH_I:
			printf("push_i %d\n", operand);
			break;
		case PUSH_V:
			printf("push_v :%d\n", operand);
			break;
		case POP_V:
			printf("pop_v :%d\n", operand);
			break;
		case PUSH_E:
			printf("push_e\n");
			break;
		case POP_E:
			printf("pop_e\n");
			break;
		case PUSH_D:
			printf("push_d [%d]\n", operand);
			break;
		case DROP:
			printf("drop\n");
			break;
		case MAKE_F:
			printf("make_f %d\n", operand);
			break;
		case SET:
			printf("set :%d\n", operand);
			break;
		case JF:
			printf("jf <%d>\n", operand);
			break;
		case JT:
			printf("jt <%d>\n", operand);
			break;
		case JMP:
			printf("jmp <%d>\n", operand);
			break;
		case CALL:
			printf("call %d\n", operand);
			break;
		case RET:
			printf("ret\n");
			break;
		case SW:
			printf("sw\n");
			break;
		case CALL_TAIL:
			printf("call-t %d\n", operand);
			break;

	}
	return 0;
}

uint32_t add_instr(opp_t opcode, uint32_t operand) {
	if (program_pointer + 5 > program_len) {
		program_len  = program_len*2 + 10;
		instr_t* nprog = realloc(program, program_len*sizeof(instr_t));
		if (nprog == NULL)
			return 0;
		program = nprog;
	}
	program[program_pointer] = (instr_t){opcode, operand};
	uint32_t ret = program_pointer;
	program_pointer++;
	return ret;
}

void del_instr(uint32_t id) {
	program_pointer--;
	printf("moving from %d to %d, n:%d\n", id+1, id, program_pointer-id);
	memmove(&program[id], &program[id+1], sizeof(instr_t)*(program_pointer-id)); 
	int32_t tmp;
	for (uint32_t i = 0; i < program_pointer; i++) {
		switch(program[i].opp_code) {
			case JMP:
			case JF:
			case JT:
				if (program[i].operand > id) {
					program[i].operand -= 1;
				}
				break;
			case MAKE_F:
				tmp = program[i].operand;
				if (tmp < 0)
					tmp = i - (-tmp) - 1;
				else
					tmp = i - tmp - 1;
				if (program[tmp].operand > id) {
					program[tmp].operand -= 1;
				}
				break;
			default:
				break;
		}
	}
}

instr_t* get_program() {
	return program;
}

uint32_t get_program_pointer() {
	return program_pointer;
}

void set_program_pointer(uint32_t new) {
	program_pointer = new;
}

void print_program() {
	uint32_t i;
	for (i = 0; i < program_pointer; i++) {
		printf("%d:\t", i);
		print_instr(program[i]);
	}
}

uint32_t get_program_length() {
	return program_pointer * (sizeof(uint32_t)+sizeof(uint8_t));
}

bool write_code(char* buffer) {
	uint32_t pos = 0;
	for (uint32_t i = 0; i < program_pointer; i++) {
		uint8_t op = program[i].opp_code;
		uint32_t ar = program[i].operand;
		memcpy(buffer+pos, &op, sizeof(uint8_t));
		pos += sizeof(uint8_t);
		memcpy(buffer+pos, &ar, sizeof(uint32_t));
		pos += sizeof(uint32_t);
	}
	return true;
}

bool read_code(char* buffer, uint32_t length) {
	program = malloc(length*sizeof(instr_t));
	if (!program)
		return false;
	uint32_t pos = 0;
	for (uint32_t i = 0; i < length; i++) {
		uint8_t opcode = 0;
		memcpy(&opcode, buffer+pos, sizeof(uint8_t));
		pos += sizeof(uint8_t);
		uint32_t operand = 0;
		memcpy(&operand, buffer+pos, sizeof(uint32_t));
		pos += sizeof(uint32_t);
		program[i] = (instr_t){.opp_code=opcode, .operand=operand};
	}
	program_pointer = length;
	program_len = length;
	return true;
}

void free_program() {
	free(program);
}
