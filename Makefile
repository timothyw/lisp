OBJS = 	tokens.o instr.o cons.o compiler.o var_table.o data_table.o macro.o \
	gc.o vm.o optimize.o env.o stack.o builtins.o load.o
MAIN := main.o

CFLAGS = -Wall -ggdb -std=gnu11

l: $(OBJS) $(MAIN)
	gcc -o l $(OBJS) $(MAIN)

.PHONY: clean

clean:
	rm -rf *.o l
