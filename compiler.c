#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "compiler.h"
#include "instr.h"
#include "macro.h"

#include "var_table.h"
#include "data_table.h"
#include "load.h"
#include "gc.h"
#include "env.h"
#include "builtins.h"
#include "vm.h"
#include "stack.h"


bool comp(data_t tokens) {
	if (tokens.type == CONS) {
		cons_t* fun_c = tokens.data;
		if (!fun_c) {
			puts("nil?");
			return false;
		}
		data_t func = fun_c->car;
		data_t args = fun_c->cdr;
		if (func.type == SYMBOL) {
			uint32_t id = func.data_i;;
			if (id != 0) {
				data_t val = get_var(*get_current_env(), id);
				if (val.type == SPECIAL) {
					func_t* special = val.data;
					uint32_t nargs = 0;
					data_t cur = args;
					for (;cur.type != NIL; nargs++) {
						if (cur.type != CONS)
							return false;
						cons_t* cur_c = cur.data;
						cur = cur_c->cdr;
					}
					if (special->arg_len != -1 && special->arg_len != nargs) {
						printf("wrong %s, takes %d arguments", get_var_table(id), special->arg_len);
						return false;
					}
					data_t* arguments = malloc(nargs*sizeof(data_t));
					cur = args;
					for (uint32_t i = 0; cur.type != NIL; i++) {
						cons_t* cur_c = cur.data;
						arguments[i] = cur_c->car;
						cur = cur_c->cdr;
					}
					data_t ret = special->function(arguments, nargs);
					free(arguments);
					return ret.type != ERR;
				} else if (val.type == MACRO) {
					//????
					// save env
					// for eah arg n > 0??:
					// 	bind var
					// bind rest ...??? if n < 0
					// set begin, find end ????
					// ee
					// ret  o
					// restore 
					// o 
					// done
					// TODO: closure????
					cons_t* old_env = *get_current_env();
					lambda_t* l = val.data;
					int32_t arg_len = l->arg_len;
					uint32_t max_args;
					if (arg_len < 0)
						max_args = -arg_len - 1;
					else
						max_args = arg_len;
					uint32_t nargs = 0;
					data_t cur = args;
					for(; cur.type != NIL && nargs < max_args; nargs++) {
						if (cur.type != CONS)
							return false;
						cons_t* cur_c = cur.data;
						printf("binding id: %d\n", abs(arg_len) - nargs-1);
						add_var(l->args[abs(arg_len) - nargs-1], cur_c->car);
						cur = cur_c->cdr;
					}
					if (cur.type != NIL) {
						if (arg_len > 0) {
							printf("macro %s takes %d args, you gave %d\n", get_var_table(id), arg_len, nargs);
							return false;
						}
					} else {
						if (nargs < arg_len) {
							printf("macro %s takes %d args, you gave %d\n", get_var_table(id), arg_len, nargs);
							return false;
						}
					}
					if (arg_len < 0) {
						add_var(l->args[0], cur);
					}
					// find end
					uint32_t start = l->addr;
					uint32_t end = get_program_pointer();
					uint32_t i = 0;
					for(i = start; i < end; i++) {
						if (get_program()[i].opp_code == SW && \
							get_program()[i+1].opp_code == RET) {
							break;
						}
					}
					end = i;
					// run??
					if (!run_program(get_program(), start, end)) {
						puts("error runnong macro:");
						printf("%s\n", get_var_table(id));
						return false;
					}
					//TODO: detect error?
					data_t c = pop();
					printf("ran macro:\n");
					print_data(c);
					printf("\n");
					// forget env:
					*get_current_env() = old_env;
					bool ret = comp(c);
					return ret;
				}
			}
		}
		if(!comp(func)) return false;
		data_t cur = args;
		uint32_t nargs = 0;
		for(;cur.type != NIL; nargs++) {
			if (cur.type != CONS)
				return false;
			cons_t* cur_c = cur.data;
			if(!comp(cur_c->car)) return false;
			cur = cur_c->cdr;
		}
		add_instr(CALL, nargs);
		return true;
	} else if (tokens.type == SYMBOL) {
		uint32_t id = tokens.data_i;
		if (id == 0) {
			puts("error, undefined var:");
			puts("????");
			return false;
		}
		add_instr(PUSH_V, id);
		return true;
	} else if (tokens.type == STR) {
		uint32_t id = add_data(data_dup(tokens));
		add_instr(PUSH_D, id);
		return true;
	} else if (tokens.type == INT) {
		add_instr(PUSH_I, tokens.data_i);
		return true;
	}
	return false; 
}

/*
if (strcmp(fname, "define") == 0) {
			lse if (strcmp(fname, "let") == 0) {
				tokens_t* bindings = func->next;
				if (bindings == NULL || bindings->type != TOK|| !bindings->next){
					puts("wrong let");
					return false;
				}
				tokens_t* body = bindings->next;
				if (body->next != NULL) {
					puts("wrong let");
					return false;
				}
				add_instr(PUSH_E, 0);
				if (bindings->type != TOK) {
					puts("wrong let2");
					return false;
				}
				tokens_t* cur = bindings->token_l;
				for (; cur != NULL; cur = cur->next) {
					if (cur->type != TOK) {
						puts("wrong let2");
						return false;
					}
					tokens_t* name = cur->token_l;
					if (name->type != CHAR || name->next == NULL) {
						puts("wrong let3");
						return false;
					}
					tokens_t* value = name->next;
					if (!comp(value)) return false;
					uint32_t id = add_var_table(name->token_c);
					add_instr(POP_V, id);
				}
				if (!comp(body)) return false;
				add_instr(SW, 0);
				add_instr(POP_E, 0);
				return true;
			} else if (strcmp(fname, "if") == 0) {
				tokens_t* cond = func->next;
				if (cond == NULL) {
					puts("if needs a condition");
					return false;
				}
				tokens_t* bodyt = cond->next;
				if (bodyt == NULL) {
					puts("if needs 2 bodies");
					return false;
				}
				tokens_t* bodyf = bodyt->next;
				if (bodyf == NULL) {
					puts("if needs a false body");
					return false;
				}
				if (!comp(cond)) return false;
				uint32_t false_jmp = add_instr(JF, 0);
				if (!comp(bodyt)) return false;
				uint32_t true_jmp = add_instr(JMP, 0);
				get_program()[false_jmp].operand = get_program_pointer();
				if (!comp(bodyf)) return false;
				get_program()[true_jmp].operand = get_program_pointer();
				return true;
			} else if (strcmp(fname, "quote") == 0) {
				tokens_t* quote = func->next;
				if (quote == NULL || quote->next != NULL) {
					puts("Invalid quote");
					return false;
				}
				uint32_t id = add_data(quote);
				add_instr(PUSH_D, id);
				return true;
			} else if (strcmp(fname, "lambda") == 0) {
				tokens_t* args = func->next;
				if (args == NULL || args->next == NULL) {
					puts("invalid lambda");
					return false;
				}
				tokens_t* body = args->next;
				if (body->next != NULL) {
					puts("invalid lambda 2");
					return false;
				}
				tokens_t* arg = NULL;
				if (args->type == TOK) {
					arg = args->token_l;
					for (;arg != NULL; arg = arg->next) {
						if (arg->type != CHAR) {
							puts("wrong arg type");
							return false;
						}
						add_var_table(arg->token_c);
					}
				} else if(args->type == CHAR) {
					add_var_table(args->token_c);
				} else {
					puts("wrong lambda");
					return false;
				}

				uint32_t jump = add_instr(JMP, 0);
				uint32_t func = get_program_pointer();
				if (!comp(body)) return false;
				add_instr(SW, 0);
				add_instr(RET, 0);
				uint32_t end = add_instr(PUSH_I, func);

				uint32_t n_args = 0;
				if (args->type == TOK) {
					arg = args->token_l;
					for (;arg != NULL; arg = arg->next, n_args++) {
						uint32_t id = lookup_var(arg->token_c);
						add_instr(PUSH_I, id);
					}
				} else {
					add_instr(PUSH_I, lookup_var(args->token_c));
					n_args = -1;
				}
				add_instr(MAKE_F, n_args);
				get_program()[jump].operand = end;
				return true;
			} else if (strcmp(fname, "load") == 0) {
				tokens_t* args = func->next;
				if (args == NULL || args->type != STR || args->next != NULL) {
					puts("wrong load");
					return false;
				}
				char* fname = args->token_c;
				load_file(fname);
				add_instr(PUSH_I, 0);
				return true;
			}
*/
