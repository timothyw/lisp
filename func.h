#ifndef FUNC_H
#define FUNC_H

#include <stdint.h>

#include "cons.h"
// arg_t?

typedef data_t (*builtin_t)(data_t*, uint32_t);

typedef struct {
	cons_t** closure;
	
	//length of args and list of arg-var ids
	int32_t arg_len;
	uint32_t* args;

	uint32_t addr;
} lambda_t;

typedef struct {
	uint32_t arg_len;
	builtin_t function;
} func_t;

#endif
