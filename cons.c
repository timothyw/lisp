#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "cons.h"
#include "gc.h"
#include "var_table.h"

cons_t* cons(data_t car, data_t cdr) {
	cons_t* ret = gc_malloc(sizeof(cons_t));
	if (!ret)
		return NULL;
	ret->car = car;
	ret->cdr = cdr;
	return ret;
}

cons_t* cons_nogc(data_t car, data_t cdr) {
	cons_t* ret = malloc(sizeof(cons_t));
	if (!ret)
		return NULL;
	ret->car = car;
	ret->cdr = cdr;
	return ret;
}

void print_cons(cons_t* c) {
	print_data(c->car);
	if (c->cdr.type == CONS) {
		printf(" ");
		print_cons(c->cdr.data);
	} else if (c->cdr.type == NIL) {
		return;
	} else {
		printf(". ");
		print_data(c->cdr);
	}
}

void print_data(data_t data) {
	switch(data.type) {
		case ERR:
			printf("ERR ");
			break;
		case INT:
			printf("%d ", data.data_i);
			break;
		/*case CHAR:
			printf(":%s; ", data.data_c);
			break;
		*/
		case SYMBOL:
			printf(":%d,%s;", data.data_i, get_var_table(data.data_i));
			break;
		case STR:
			printf("\"%s\" ", data.data_c);
			break;
		case ENV:
			printf("ENV");
			break;
		case CONS:
			printf("(");
			print_cons(data.data);
			printf(")");
			break;
		case LAMBDA:
			printf("LAMBDA ");
			break;
		case FUNC:
			printf("FUNC ");
			break;
		case SPECIAL:
			printf("SP MACRO ");
			break;
		case MACRO:
			printf("MACRO ");
			break;
		case NIL:
			printf("()");
			break;
	}
}

uint32_t get_data_length(data_t data) {
	switch(data.type) {
		case INT:
		case SYMBOL:
			return 2*sizeof(uint32_t);
		case STR:
			return sizeof(uint32_t) + strlen(data.data) + 1;
		case CONS:
			return	sizeof(uint32_t) +  \
				get_data_length(((cons_t*)data.data)->car) + \
				get_data_length(((cons_t*)data.data)->cdr);
		default:
			return sizeof(uint32_t);
	}
}

uint32_t write_data(char* buffer, data_t data) {
	uint32_t type = data.type;
	memcpy(buffer, &type, sizeof(uint32_t));
	uint32_t pos = sizeof(uint32_t);
	uint32_t tmp;
	switch(data.type) {
		case INT:
		case SYMBOL:
			memcpy(buffer+pos, &data.data_i, sizeof(uint32_t));
			return pos+sizeof(uint32_t);
		case STR:
			tmp = strlen(data.data)+1;
			memcpy(buffer+pos, data.data, tmp);
			return pos+tmp;
		case CONS:
			pos += write_data(buffer+pos, ((cons_t*)data.data)->car);
			pos += write_data(buffer+pos, ((cons_t*)data.data)->cdr);
			return pos;
		default:
			return pos;
	}
}

data_t read_data(char* buffer, uint32_t * len) {
	uint32_t type = 0;
	memcpy(&type, buffer, sizeof(uint32_t));
	uint32_t pos = sizeof(uint32_t);
	uint32_t d = 0;
	data_t a;
	data_t b;
	switch(type) {
		case INT:
		case SYMBOL:
			memcpy(&d, buffer+pos, sizeof(uint32_t));
			pos+= sizeof(uint32_t);
			*len = pos;
			return (data_t){.type=type, .data_i=d};
		case STR:
			d = strlen(buffer+pos);
			a.data = malloc(d+1);
			a.type = STR;
			memcpy(a.data, buffer+pos, d+1);
			*len = pos+d+1;
			return a;
		case CONS:
			a = read_data(buffer+pos, &d);
			pos += d;
			b = read_data(buffer+pos, &d);
			pos += d;
			*len = pos;
			return (data_t){.type=CONS,.data=cons_nogc(a, b)};
		default:
			*len = pos;
			return (data_t) {.type = type, .data=0};
	}
}

//use only if all ref data is not gc!!
void free_data_nogc(data_t dat) {
	if (dat.type == CONS) {
		cons_t* c = dat.data;
		free_data_nogc(c->car);
		free_data_nogc(c->cdr);
		free(c);
	} else if (dat.type == STR) {
		free(dat.data);
	} /*else if (dat.type == CHAR) {
		free(dat.data);
	}*/
}


data_t data_dup(data_t orig) {
	if (orig.type == CONS) {
		cons_t* old = orig.data;
		cons_t* new = cons_nogc(data_dup(old->car), data_dup(old->cdr));
		return (data_t){.type=CONS, .data=new};
	} if (/*orig.type == CHAR || */orig.type == STR) {
		return (data_t){.type=orig.type, .data=strdup(orig.data)};
	}
	return orig;
}
