#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>

#include "stack.h"

data_t* stack = NULL;
uint32_t stack_pointer = 0;
uint32_t stack_len = 0;

data_t* get_stack() {
	return stack;
}

uint32_t get_stack_len() {
	return stack_pointer;
}

bool push(data_t val) {
	if (stack_pointer + 5 > stack_len) {
		stack_len = 2*stack_len + 10;
		data_t* new_stack = realloc(stack, stack_len*sizeof(data_t));
		if (new_stack == NULL)
			return false;
		stack = new_stack;
	}
	stack[stack_pointer++] = val;
	return true;
}

data_t pop() {
	if (stack_pointer < 1)
		return (data_t){0};
	return stack[--stack_pointer];
}

void sw() {
	if (stack_pointer < 2)
		return;
	data_t tmp = stack[stack_pointer-1];
	stack[stack_pointer-1] = stack[stack_pointer-2];
	stack[stack_pointer-2] = tmp;
}

void print_stack() {
	
	for (uint32_t i = stack_pointer-1; i+1 > 0; i--) {
		printf("%d: ", stack_pointer-i);
		print_data(stack[i]);
		printf("\n");
	}
}

void free_stack() {
	//no need to free data: gc
	free(stack);
	stack = NULL;
}
