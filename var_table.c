#include <string.h>
#include <stdlib.h>

#include "var_table.h"

char** var_table = NULL;
uint32_t var_table_size = 0;
uint32_t var_pointer = 0;

char* special_table[] = {
	"\0",
	"define",
	"set!",
	"begin",
	"let",
	"if",
	"quote",
	"lambda",
	"load",
	"define-macro",
	"or",
	"and",
	"print",
	"cons",
	"car",
	"cdr",
	"atom?",
	"int?",
	"eq?",
	"nil?",
	"string?",
	"+",
	"-",
	"*",
	"/",
	"=",
	"==",
	"equals?",
	"<=",
	">=",
	">",
	"<",
	"not",
	"get-char",
	"str-len",
	"sub-string",
	"set-car!",
	"set-cdr!"
	//== <= => > < ...
};

uint32_t special_length = sizeof(special_table) / sizeof(char*);

uint32_t add_var_table(char* name) {
	uint32_t pre = lookup_var(name);
	if (pre != 0)
		return pre;
	if (var_pointer + 5 > var_table_size) {
		var_table_size = var_table_size * 2 + 10;
		char** new_table = realloc(var_table, var_table_size*sizeof(char*));
		if (new_table == NULL)
			return 0;
		var_table = new_table;
	}
	var_table[var_pointer] = strdup(name); //dup
	uint32_t ret = var_pointer;
	var_pointer++;
	return ret+101;
}

uint32_t lookup_var(char* name) {
	//look for standard funcs:
	for (uint32_t i = 0; i < special_length; i++) {
		if (strcmp(special_table[i], name) == 0)
			return i;
	}
	for (uint32_t i = 0; i < var_pointer; i++) {
		if (strcmp(var_table[i], name) == 0)
			return i+101;
	}
	return 0;
}

char* get_var_table(uint32_t id) {
	if (id < 101)
		return special_table[id];
	else
		return var_table[id-101];
}

uint32_t get_var_length() {
	uint32_t len  = sizeof(uint32_t);
	for (uint32_t i = 0; i < var_pointer; i++) {
		len += strlen(var_table[i])+1;
	}
	return len;
}

bool write_var_table(char* buffer) {
	uint32_t pos = sizeof(uint32_t);
	memcpy(buffer, &var_pointer, sizeof(uint32_t));
	for (uint32_t i = 0; i < var_pointer; i++) {
		uint32_t len = strlen(var_table[i]);
		memcpy(buffer+pos, var_table[i], len+1);
		pos += len+1;
	}
	return true;
}

bool read_var_table(char* buffer) {
	uint32_t var_length  = 0;
	memcpy(&var_length, buffer, sizeof(uint32_t));
	var_table = malloc(var_length*sizeof(char*));
	var_table_size = var_length;
	var_pointer = var_length;
	uint32_t pos = sizeof(uint32_t);
	for (uint32_t i = 0; i < var_length; i++) {
		uint32_t len = strlen(buffer+pos);
		char* str = malloc(len+1);
		memcpy(str, buffer+pos, len+1);
		var_table[i] = str;
		pos += len+1;
	}
	return true;
}

void free_var_table() {
	for (uint32_t i = 0; i < var_pointer; i++) {
		free(var_table[i]);
	}
	free(var_table);
}
