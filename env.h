#ifndef ENV_H
#define ENV_H
#include <stdbool.h>
#include <stdint.h>

#include "cons.h"

cons_t** current_env;

cons_t** get_current_env();

void set_current_env(cons_t** new_env);

cons_t** new_env(cons_t** old_env);
bool add_var(uint32_t id, data_t val);
data_t get_var(cons_t* env, uint32_t id);
data_t set_var(cons_t* env, uint32_t id, data_t val);

#endif
