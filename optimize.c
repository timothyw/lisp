#include <stdint.h>

#include "optimize.h"
#include "instr.h"
#include "type.h"

void drop_opt() {
	instr_t* program = get_program();
	uint32_t len = get_program_pointer();
	for (uint32_t i = 1; i < len; i++) {
		uint32_t n = i;
		if (program[n].opp_code == DROP || (program[n].opp_code == NOP && \
						   program[n].operand == DROP)){
			switch (program[i-1].opp_code){
				case PUSH_I:
				case PUSH_V:
				case PUSH_D:
				case PUSH_E:
					program[n].opp_code = NOP;
					program[n].operand = DROP;
					program[i-1].opp_code = NOP;
					program[i-1].operand = 0;
					break;
				default:
					break;
			}
		}
	}
	//for program:
	//	if instr == drop && instr-1 == push_*:
	//		instr-1 = nop
	//		instr = nop
}

void tail_opt() {
	//for instr in program:
	//	if instr == call
	//		if instr+1 == sw && instr+2 == ret || instr+1 == jmp && isntr+j == ..
	//		...
	instr_t* program = get_program();
	uint32_t len = get_program_pointer();
	for (uint32_t i = 0; i < len-2; i++) {
		if (program[i].opp_code == CALL) {
			if (program[i+1].opp_code == SW && program[i+2].opp_code == RET) {
				program[i].opp_code = CALL_TAIL;
			} else if (program[i+1].opp_code == JMP) {
				uint32_t n = program[i+1].operand;
				if (program[n].opp_code == SW && program[n+1].opp_code == RET) {
					program[i].opp_code = CALL_TAIL;
				}
			}
		}
	}
}

void nop_opt() {

	//for instrin program:
	//	if instr == nop:
	//		del_instr(instr);
	instr_t* program = get_program();
	for (uint32_t i = 0; i < get_program_pointer(); i ++) {
		if (program[i].opp_code == NOP) {
			del_instr(i);
			i--;
		}
	}
}

void macro_opt() {
	//for insr in program:
	//	if instr == NOP && op == MACRO && next == JMP
	//		replace all NOP
	instr_t* program = get_program();
	for (uint32_t i = 0; i < get_program_pointer(); i++) {
		if (program[i].opp_code == NOP && program[i].operand == MACRO && \
			program[i+1].opp_code == JMP) {
			
			uint32_t start = i+1;
			uint32_t end = program[start].operand ;
			for (uint32_t j = start; j < end; j++) {
				program[j].opp_code = NOP;
			}
		}
	}
}

void optimize() {
	//TODO: take out defmacros..
	drop_opt();
	tail_opt();
	macro_opt();
	print_program();
	nop_opt();
}
