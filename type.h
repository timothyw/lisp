#ifndef TYPE_H
#define TYPE_H
enum Type {
	ERR = 0,
	INT,
	//CHAR,
	SYMBOL,
	STR,
	ENV,
	CONS,
	FUNC,
	LAMBDA,
	SPECIAL,
	MACRO,
	NIL
};
#endif
