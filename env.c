#include <stdlib.h>

#include "env.h"
#include "gc.h"

cons_t** current_env;

cons_t** get_current_env() {
	return current_env;
}

void set_current_env(cons_t** new_env) {
	current_env = new_env;
}

cons_t** new_env(cons_t** old_env) {
	cons_t** ret = gc_malloc(sizeof(cons_t*));
	data_t search;
	if (old_env) {
		search = (data_t){.type=ENV, .data=old_env};
	} else {
		search = (data_t){.type=NIL, .data=0};			
	}
	cons_t* first = cons(search, (data_t){.type=NIL,.data=0});
	*ret = first;
	return ret;

}

bool add_var(uint32_t id, data_t val) {
	data_t id_d = {.type=INT, .data_i = id};
	data_t val_cons = {.type=CONS, .data=cons(id_d, val)};
	data_t current = {.type=CONS, .data=*current_env};
	cons_t* new_env = cons(val_cons, current);
	*current_env = new_env;
	return true;
}

data_t get_var(cons_t* env, uint32_t id) {
	// search in current env:
	// if car(env).type == CONS:
	// 	compare id to car(car(env)) -> ret cdr(car(env))
	// else if car(env).type == ENV:
	// 	...
	// if cdr(env).type == CONS:
	// 	search in cdr(env).data???
	if (env->car.type == CONS) {
		cons_t* id_val = env->car.data;
		if (id == id_val->car.data_i) {
			return id_val->cdr;
		}
	} else if (env->car.type == ENV) {
		// look in parent env:
		cons_t** search = env->car.data;
		return get_var(*search, id);
	} //else ???

	if (env->cdr.type == CONS) {
		cons_t* next = env->cdr.data;
		return get_var(next, id);
	}
	return (data_t){0};
}

data_t set_var(cons_t* env, uint32_t id, data_t val) {
	if (env->car.type == CONS) {
		cons_t* id_val = env->car.data;
		if (id == id_val->car.data_i) {
			data_t ret = id_val->cdr;
			id_val->cdr = val;
			return ret;
		}
	} else if (env->car.type == ENV) {
		// look in parent env:
		cons_t** search = env->car.data;
		return set_var(*search, id, val);
	} //else ???

	if (env->cdr.type == CONS) {
		cons_t* next = env->cdr.data;
		return set_var(next, id, val);
	}
	return (data_t){0};
}
